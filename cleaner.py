import csv, requests

with open("dexTable-dev.csv", newline='') as dex:
    dexReader = csv.DictReader(dex)
    currentNum = 1
    for row in dexReader:
        # print(row['Name'], row['Link'])
        name = row['Name']
        link = row['Link']
        image = requests.get(link, allow_redirects=False)
        open("Dex/"+str(currentNum)+" - "+name+".png", 'wb').write(image.content)
        currentNum+= 1
